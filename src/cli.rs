use crate::command::Command;
use crate::command::Commands;
use crate::context::Context;

use clap::Parser;
use std::env;
use std::path::PathBuf;

/// Gamemaster tools all in one
#[derive(Debug, Parser)]
#[command(name = "gamemaster-tools")]
#[command(about = "Gamemaster tools all in one", long_about = None)]
pub struct Cli {
  /// Set a config file
  #[arg(short = 'c', long = "config", value_name = "FILE")]
  config_path: Option<PathBuf>,

  #[command(subcommand)]
  command: Commands,
}

impl Cli {
  /// Tools run entry.
  pub fn run() {
    let args = Cli::parse();

    let config_path = match args.config_path {
      Some(config_path) => config_path,
      None => env::current_dir().unwrap().join("config/tools.yaml"),
    };

    let ctx = Context::new(config_path);

    match args.command {
      Commands::Git(git) => git.execute(&ctx),
    }
  }
}
