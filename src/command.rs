use crate::context::Context;
use crate::git::command::GitCommands;

use clap::Subcommand;
use log::error;

/// Defines how commands are executed.
pub trait Command {
  /// Command execution.
  fn execute(&self, ctx: &Context) {
    error!("no implementation, ctx:{:?}", ctx);
  }
}

#[derive(Debug, Subcommand)]
pub enum Commands {
  Git(GitCommands),
}
