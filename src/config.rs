use crate::context::Context;
use crate::git::config::GitConfig;

use serde::Deserialize;

/// Config.
#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
#[allow(unused)]
pub struct Config {
  // Git config.
  pub git: GitConfig,
}

impl Config {
  /// Create a config.
  pub fn new(ctx: &Context) -> Self {
    let config = config::Config::builder()
      .add_source(config::File::from(ctx.config_path.clone()))
      .build()
      .unwrap();

    config.try_deserialize().unwrap()
  }
}
