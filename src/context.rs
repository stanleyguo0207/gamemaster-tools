use crate::config::Config;

use std::path::PathBuf;

/// Global context.
#[derive(Debug)]
pub struct Context {
  /// Config path.
  pub config_path: PathBuf,
  /// Config.
  pub config: Option<Config>,
}

impl Context {
  /// Create a global context.
  pub fn new(config_path: PathBuf) -> Self {
    let mut ctx = Context {
      config_path: config_path,
      config: None,
    };

    // Init log4rs.
    crate::log::init(&ctx);

    // Init config.
    ctx.config = Some(Config::new(&ctx));

    ctx
  }
}
