use crate::command::Command;
use crate::context::Context;
use crate::git::sub_command::GitSubCommands;

use clap::Args;

/// Git related tools
#[derive(Debug, Args)]
#[command(about = "Git related tools", long_about = None)]
pub struct GitCommands {
  #[command(subcommand)]
  command: GitSubCommands,
}

impl Command for GitCommands {
  /// Command execution.
  fn execute(&self, ctx: &Context) {
    match &self.command {
      GitSubCommands::Clone(clone) => clone.execute(&ctx),
      GitSubCommands::Pull(pull) => pull.execute(&ctx),
    }
  }
}
