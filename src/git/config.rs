use serde::Deserialize;

use std::path::PathBuf;

/// Git repository info.
#[derive(Debug, Deserialize)]
#[allow(unused)]
pub struct Repository {
  /// The url of Repository.
  pub url: String,
  /// The branch of Repository.
  pub branch: Option<String>,
  /// Whether has sub repository.
  pub recursive: Option<bool>,
}

/// Git local info.
#[derive(Debug, Deserialize)]
#[allow(unused)]
pub struct Local {
  // The Directory of Local repository.
  pub dir: PathBuf,
}

/// Git ssh info.
#[derive(Debug, Deserialize)]
#[allow(unused)]
pub struct Ssh {
  // The private key of ssh.
  pub private_key: String,
}

/// Git config.
#[derive(Debug, Deserialize)]
#[allow(unused)]
pub struct GitConfig {
  /// Git repository config.
  pub repository: Repository,
  /// Git local config.
  pub local: Local,
  /// Git ssh config.
  pub ssh: Ssh,
}
