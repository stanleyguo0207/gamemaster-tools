mod clone;
mod pull;

use clone::GitSubCommandsClone;
use pull::GitSubCommandsPull;

use clap::Subcommand;

#[derive(Debug, Subcommand)]
pub enum GitSubCommands {
  Clone(GitSubCommandsClone),
  Pull(GitSubCommandsPull),
}
