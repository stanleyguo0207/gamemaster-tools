use crate::command::Command;
use crate::context::Context;

use clap::Args;

/// Git pull
#[derive(Debug, Args)]
#[command(args_conflicts_with_subcommands = true)]
pub struct GitSubCommandsPull {}

impl Command for GitSubCommandsPull {
  // Git clone command
  fn execute(&self, _ctx: &Context) {
    // let git_config = &ctx.config.as_ref().unwrap().git;
    // let url: &str = git_config.repository.url.as_ref();
    // let local_path = Path::new(git_config.local.dir.as_str());

    // let repo = if !local_path.exists() {
    //   Repository::clone(url, local_path).unwrap()
    // } else {
    //   Repository::open(local_path).unwrap()
    // };

    // if git_config.repository.recursive {
    // } else {
    // }
  }
}
