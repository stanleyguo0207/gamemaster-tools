use crate::context::Context;
use log::info;

/// Init log4rs.
pub fn init(ctx: &Context) {
  let config_dir = ctx.config_path.parent().unwrap();
  let config_file = config_dir.join("tools-log.yaml");
  log4rs::init_file(&config_file, Default::default()).unwrap();

  info!("use log config file: {}", config_file.to_str().unwrap())
}
