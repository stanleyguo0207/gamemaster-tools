mod cli;
mod command;
mod config;
mod context;
mod git;
mod log;

use cli::Cli;

fn main() {
  Cli::run()
}
